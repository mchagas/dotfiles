let g:vim_bootstrap_editor = "vim"

"" Include colorscheme set wide (Terminal + VIm)
if filereadable(expand("~/.vimrc_background"))
	let g:base16colorspace=256
	source ~/.vimrc_background
endif

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default'

""  indentLine
let g:indentLine_enabled = 1
let g:indentLine_setColors = 0
let g:indentLine_char = '┆'
let g:indentLine_faster = 1

"let g:loaded_fugitive = 1
"set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

if exists("*fugitive#statusline")
	set statusline+=%{fugitive#statusline()}
endif

"" Gitgutter in statusline
function! GitStatus()
	return join(filter(map(['A','M','D'], {i,v -> v.': '.GitGutterGetHunkSummary()[i]}), 'v:val[-1:]'), ' ')
endfunction


"" snippets
let g:UltiSnipsExpandTrigger="<Tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"
let g:UltiSnipsEditSplit="vertical"

"" syntastic
let g:syntastic_always_populate_loc_list=1
let g:syntastic_error_symbol='✗'
let g:syntastic_warning_symbol='⚠'
let g:syntastic_style_error_symbol = '✗'
let g:syntastic_style_warning_symbol = '⚠'
let g:syntastic_auto_loc_list=1
let g:syntastic_aggregate_errors = 1

"" Tagbar
"nmap <silent> <F4> :TagbarToggle<CR>
"let g:tagbar_autofocus = 1
"
"
"" Run the Flake8 check every time you write a Python file
autocmd BufWritePost *.py call flake8#Flake8()

"" Python Syntax
let g:python_highlight_all = 1

"" Python Indent
let g:python_pep8_indent_multiline_string = -2
let g:python_pep8_indent_hang_closing = 1

let g:python_version = matchstr(system("python --version | cut -f2 -d' '"), '^[0-9]')
if g:python_version =~ 3
    let g:python_host_prog = "/usr/bin/python"
else
    let g:python3_host_prog = "/usr/bin/python3"
endif

"" vim-python
augroup vimrc-python
autocmd!
autocmd FileType python setlocal
	\ expandtab
	\ shiftwidth=4
	\ tabstop=8
	\ colorcolumn=79
    \ formatoptions+=croq
	\ softtabstop=4
	\ smartindent
	\ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
augroup END

"" What to use for an indent.
"" This will affect Ctrl-T and 'autoindent'.
" Python: 4 spaces
" C: tabs (pre-existing files) or 4 spaces (new files)
au BufRead,BufNewFile *.py,*pyw set shiftwidth=4
au BufRead,BufNewFile *.py,*.pyw set expandtab
function Select_c_style()
    if search('^\t', 'n', 150)
        set shiftwidth=8
        set noexpandtab
    el
        set shiftwidth=4
        set expandtab
    en
endfunction
au BufRead,BufNewFile *.c,*.h call Select_c_style()
au BufRead,BufNewFile Makefile* set noexpandtab

"" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red

"" Display tabs at the beginning of a line in Python mode as bad.
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
"" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

"" Wrap text after a certain number of characters
"" Python: 79
"" C: 79
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h set textwidth=79

"" Turn off settings in 'formatoptions' relating to comment formatting.
"" - c : do not automatically insert the comment leader when wrapping based on
""    'textwidth'
"" - o : do not insert the comment leader when using 'o' or 'O' from command mode
"" - r : do not insert the comment leader when hitting <Enter> in insert mode
"" Python: not needed
"" C: prevents insertion of '*' at the beginning of every line in a comment
au BufRead,BufNewFile *.c,*.h set formatoptions-=c formatoptions-=o formatoptions-=r

"" Use UNIX (\n) line endings.
"" Only used for new files so as to not force existing files to change their
"" line endings.
"" Python: yes
"" C: yes
au BufNewFile *.py,*.pyw,*.c,*.h set fileformat=unix

highlight LiteralTabs ctermbg=darkgreen guibg=darkgreen
match LiteralTabs /\s\  /
highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
match ExtraWhitespace /\s\+$/

"let g:deoplete#enable_at_startup = 1
"
"" jedi-vim
"let g:jedi#popup_on_dot = 0
let g:jedi#goto_assignments_command = "<leader>g"
let g:jedi#goto_definitions_command = "<leader>d"
let g:jedi#documentation_command = "K"
let g:jedi#usages_command = "<leader>n"
let g:jedi#rename_command = "<leader>r"
let g:jedi#show_call_signatures = "0"
let g:jedi#completions_command = "<C-Space>"
let g:jedi#smart_auto_mappings = 0
"
""" syntastic
"let g:syntastic_python_checkers=['python', 'flake8']
"let g:syntastic_puppet_checkers=['puppet']
"
"
"" Set up Puppet manifest and spec options
au FileType puppet setlocal tabstop=8 expandtab shiftwidth=2 softtabstop=2
au BufRead,BufNewFile *.pp
  \ set filetype=puppet
au BufRead,BufNewFile *_spec.rb
  \ nmap <F8> :!rspec --color %<CR>

"" vim-markdown
"let g:vim_markdown_folding_disabled = 1 " Disable folding
"
"" Allow for TOC window to auto-fit when it's possible to it to shrink
"let g:vim_markdown_toc_autofit = 1
"
"" fenced code block
"let g:vim_markdown_fenced_languages = ['viml=vim', 'python=py']
"
"" used as $x^2$, $$x^2$$, escapable as \$x\$ and \$\$x\$\$
"let g:vim_markdown_math = 1
"
"let g:AutoPairsFlyMode = 1
"let g:AutoPairsShortcutBackInsert = '<M-b>'
"
"
""au FileType python let b:AutoPairs = AutoPairsDefine({"(" : ")", '"':'"', "'":"'"})
"let g:DelimitMateOn = 1

let make = 'make'
if system('uname -o') =~ '^GNU/'
	let make = 'make'
endif

