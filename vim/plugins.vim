call plug#begin('~/.vim/plugged')

let vimplug_exists=expand('~/.vim/autoload/plug.vim')

if !filereadable(vimplug_exists)
	 echo "Installing Vim-Plug..."
	 echo ""
  	 silent !\curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
		 https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	let g:not_finish_vimplug = "yes"
	autocmd vimenter * pluginstall
endif

" *****************************************************************************
" Plug install packages
" *****************************************************************************
Plug 'chriskempson/base16-vim'                      " base16 themes
Plug 'vim-airline/vim-airline'                      " Vim statusline
Plug 'vim-airline/vim-airline-themes'               " Vim statusline themes
Plug 'yggdroot/indentline'							" Show indentation level
Plug 'tpope/vim-fugitive'                           " git <3
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-surround'
Plug 'airblade/vim-gitgutter'                       " show what lines have changed when inside a git repo
"Plug 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
" Plug 'plasticboy/vim-markdown', {'for': 'markdown'} " Markdown vim mode
"Plug 'sheerun/vim-polyglot'                         " Collection of language packs for Vim
"Plug 'honza/vim-snippets'
"Plug 'sirver/ultisnips'
" "Python Bundle
"Plug 'davidhalter/jedi-vim'
"Plug 'vim-python/python-syntax'
"Plug 'hynek/vim-python-pep8-indent'
"Plug 'nvie/vim-flake8'
" " Puppet Bundle
Plug 'rodjek/vim-puppet'
" Plug 'godlygeek/tabular'                            " Auto align => for Puppet code
" Plug 'ekalinin/dockerfile.vim'
" Plug 'bronson/vim-trailing-whitespace'
" Plug 'raimondi/delimitmate'
" Plug 'scrooloose/syntastic'
" Plug 'jeffkreeftmeijer/vim-numbertoggle'            " Relative and absolute numbers
" Plug 'neomake/neomake'                              " lint checker
" "Vim-Session
" Plug 'xolox/vim-misc'
" Plug 'xolox/vim-session'
" Plug 'Shougo/vimproc.vim', {'do': 'make'}
" "Include user's extra bundle
" if filereadable(expand("~/.vim/local.bundles"))
"   source ~/.vim/local.bundles
" endif
""nvim autocomplete
"if has('nvim')
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"else
" Plug 'roxma/nvim-yarp'
" Plug 'roxma/vim-hug-neovim-rpc'
" Plug 'Shougo/deoplete.nvim'
" endif
call plug#end()



