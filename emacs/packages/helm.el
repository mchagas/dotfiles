;; helm mode
(use-package helm
  :bind (("M-x" . helm-M-x)
          ("C-x C-f" . helm-find-files)
          ("C-x f" . helm-recentf)
          ("M-y" . helm-show-kill-ring)
          ("C-x b" . helm-mini)
          ("C-c h o" . helm-occur))
  :bind (:map helm-map
          ("M-i" . helm-previous-line)
          ("M-k" . helm-next-line)
          ("M-I" . helm-previous-page)
          ("M-K" . helm-next-page)
          ("M-h" . helm-beginning-of-buffer)
          ("M-H" . helm-end-of-buffer))
  :ensure t
  :config (progn
            (setq helm-buffers-fuzzy-matching t)
            (setq helm-grep-ag-command "rg --color=always --colors 'match:fg:black' --colors 'match:bg:yellow' --smart-case --no-heading --line-number %s %s %s")
            (setq helm-grep-ag-pipe-cmd-switches '("--colors 'match:fg:black'" "--colors 'match:bg:yellow'"))
            (setq helm-grep-default-command
              "ack-grep -Hn --color --smart-case --no-group %e %p %f"
              helm-grep-default-recurse-command
              "ack-grep -H --color --smart-case --no-group %e %p %f")
            (setq helm-ls-git-grep-command
              "git grep -n%cH --color=always --full-name -e %p %f")
            (setq helm-M-x-fuzzy-match t
              helm-bookmark-show-location t
              helm-buffers-fuzzy-matching t
              helm-completion-in-region-fuzzy-match t
              helm-file-cache-fuzzy-match t
              helm-imenu-fuzzy-match t
              helm-mode-fuzzy-match t
              helm-locate-fuzzy-match t
              helm-quick-update t
              helm-recentf-fuzzy-match t
              helm-apropos-fuzzy-match t
              helm-semantic-fuzzy-match t)
            (helm-autoresize-mode 1)
            (helm-mode 1)))

(use-package helm-ls-git
  :bind (("C-M-;"   . helm-ls-git-ls)
          ("C-x C-d" . helm-browse-project)))

(use-package helm-projectile
  :ensure t)
