(use-package wakatime-mode
  :custom (wakatime-cli-path "~/.pyenv/shims/wakatime")
  :config (global-wakatime-mode))
