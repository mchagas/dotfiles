(use-package company
  :diminish company-mode
  :config (global-company-mode t))
