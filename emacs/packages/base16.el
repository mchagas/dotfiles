;; base16 theme
(use-package base16-theme
  :ensure t
  :config
  (load-theme 'base16-monokai t))
