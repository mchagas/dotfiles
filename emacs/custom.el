(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(evil-collection-setup-minibuffer t)
 '(org-journal-date-format "%A, %d %B %Y" t)
 '(org-journal-dir "~/orgs/journal/" t)
 '(package-selected-packages
   (quote
    (pyenv-mode-auto evil-goggles evil-visualstar evil-collection evil-expat disable-mouse auto-package-update diminish mode-icons org-journal adoc-mode all-the-icons-dired avy highlight-indent-guides exec-path-from-shell yaml-mode markdown-mode mode-line-bell smart-mode-line-atom-one-dark-theme smart-mode-line flycheck editorconfig evil-leader wakatime-mode pyenv-mode elpy rainbow-delimiters base16-theme ripgrep smex flx all-the-icons-ivy ivy-rich counsel-etags counsel-projectile counsel evil-magit git-gutter-fringe+ magit which-key try use-package)))
 '(wakatime-cli-path "~/.pyenv/shims/wakatime"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(evil-goggles-change-face ((t (:inherit diff-removed))))
 '(evil-goggles-delete-face ((t (:inherit diff-removed))))
 '(evil-goggles-paste-face ((t (:inherit diff-added))))
 '(evil-goggles-undo-redo-add-face ((t (:inherit diff-added))))
 '(evil-goggles-undo-redo-change-face ((t (:inherit diff-changed))))
 '(evil-goggles-undo-redo-remove-face ((t (:inherit diff-removed))))
 '(evil-goggles-yank-face ((t (:inherit diff-changed)))))
